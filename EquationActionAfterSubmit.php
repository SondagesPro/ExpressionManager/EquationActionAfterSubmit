<?php

/**
 * EquationActionAfterSubmit : Act like an equation after survey submitted, allow to set some value.
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2022 Denis Chenu / SondagesPro <https://sondages.pro/>
 * @license AGPL
 * @version 0.3.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Afeor General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */

class EquationActionAfterSubmit extends PluginBase
{
    protected $storage = 'DbStorage';

    protected static $description = 'Act like an equation after survey submitted';
    protected static $name = 'EquationActionAfterSubmit';

    /** @inheritdoc, this plugin didn't have any public method */
    public $allowedPublicMethods = array();

    public function init()
    {
        $this->subscribe('afterSurveyComplete');
        $this->subscribe('beforeSurveySettings');
        $this->subscribe('newSurveySettings');
    }

    /**
     * Register the settings
     */
    public function beforeSurveySettings()
    {
        $oEvent = $this->event;
        $surveyId = $oEvent->get('survey');
        $current = $this->get('expression', "Survey", $surveyId, "");
        $help = $this->gT("You need to add the curly brace for action.");
        if ((strpos($current, "{") !== false) ) {
            $help = sprintf($this->gT("Actual action: %s"), "<pre style='white-space: pre-wrap;'>" . $this->getHtmlExpression($current, $surveyId) . "</pre>");
        }
        $oEvent->set(
            "surveysettings.{$this->id}",
            array(
                'name' => get_class($this),
                'settings' => array(
                    'expression' => array (
                        'type' => 'text',
                        'label' => $this->gT("Expression to do after submit"),
                        'htmlOptions' => array(
                            'rows' => 1
                        ),
                        'help' => $help,
                        'current' => $current
                    ),
                    'otherEquationAfterSubmit' => array (
                        'type' => 'boolean',
                        'label' => $this->gT("Action of equation question after submit."),
                        'help' => $this->gT("If equation quetsion use result updated on this equation, you can need to laucnh it again"),
                        'current' => $this->get('otherEquationAfterSubmit', "Survey", $surveyId, false)
                    ),
                )
            )
        );
    }

    /** @inheritdoc */
    public function newSurveySettings()
    {
        foreach ($this->event->get('settings') as $name => $value) {
            $this->set($name, $value, 'Survey', $this->event->get('survey'), "");
        }
    }

    /** @inheritdoc */
    public function afterSurveyComplete()
    {
        $surveyId   = $this->event->get('surveyId');
        $responseId   = $this->event->get('responseId');
        $expression = trim($this->get('expression', "Survey", $surveyId, ""));
        if (empty($expression)) {
            return;
        }
        $BeforeUpdatedValues = self::ownGetUpdatedvalues();
        $result = LimeExpressionManager::ProcessStepString($expression, [], 3, 1);
        if ($this->get('otherEquationAfterSubmit', "Survey", $surveyId, false)) {
            /* get all equation questions */
            $oQuestions = Question::model()->with('group')->findAll([
                'condition' => 't.sid = :sid AND type = :type AND parent_qid = 0',
                'order' => 'group_order asc, question_order asc',
                'params' => array(
                    ':sid' => $surveyId,
                    ':type' => '*'
                )
            ]);
            foreach ($oQuestions as $oQuestion) {
                /* Find equation attribute */
                $equation = null;
                $oEquation = QuestionAttribute::model()->find(
                    "qid = :qid and attribute = :attribute",
                    [ ":qid" => $oQuestion->qid, ":attribute" => 'equation' ]
                );
                if (empty($oEquation) || empty($oEquation->value)) {
                    $oQuestionL10n = QuestionL10n::model()->find(
                        'qid = :qid AND language = :language',
                        [ ":qid" => $oQuestion->qid, ":language" => App()->getLanguage() ]
                    );
                    if (!empty($oQuestionL10n)) {
                        $equation = $oQuestionL10n->question;
                    }
                } else {
                    $equation = $oEquation->value;
                }
                if($equation) {
                    $sgq = $surveyId . "X" . $oQuestion->gid . "X" . $oQuestion->qid;
                    $newValue = LimeExpressionManager::ProcessStepString(
                        $equation,
                        ['QID' => $oQuestion->qid, 'GID' => $oQuestion->gid, 'SGQ' => $sgq],
                        3,
                        1
                    );
                    $oIsNumeric = QuestionAttribute::model()->find(
                        "qid = :qid and attribute = :attribute",
                        [ ":qid" => $oQuestion->qid, ":attribute" => 'numbers_only' ]
                    );
                    if (!empty($oIsNumeric) && !empty($oIsNumeric->value) && !is_numeric($newValue)) {
                        $newValue = "";
                    }
                    /* Set the value for other equation */
                    $_SESSION['survey_' . $surveyId][$sgq] = $newValue;
                    /* Put the value in updatedvar */
                    LimeExpressionManager::singleton()->SetVariableValue (
                        "=",
                        $sgq,
                        $newValue
                    );
                }
            }
        }
        $AfterUpdatedValues = self::ownGetUpdatedvalues();
        $NewUpdatedValues = array_diff_assoc(
            $AfterUpdatedValues,
            $BeforeUpdatedValues
        );
        if (empty($NewUpdatedValues)) {
            return;
        }
        $oResponse = Response::model($surveyId)->findByPk($responseId);
        if (empty($oResponse)) {
            return;
        }
        $oResponse->setAttributes($NewUpdatedValues, false);
        if (App()->getConfig('versionnumber') < 4) {
            if (!$oResponse->save()) {
                $this->log("Unable to update values for {$responseId} in {$surveyId}", 'error');
            }
            return;
        }
        $oResponse->decrypt();
        if (!$oResponse->encryptSave()) {
            $this->log("Unable to update values for {$responseId} in {$surveyId}", 'error');
        }
    }

    /**
     * get the udated values vy Expression
     * Workaround for 3.X
     * @return arry
     */
    private static function ownGetUpdatedvalues()
    {
        if (App()->getConfig('versionnumber') < 4) {
            $surveyId = LimeExpressionManager::getLEMsurveyId();
            $updatedValues = array();
            $fieldNames = $_SESSION['survey_' . $surveyId]['fieldnamesInfo'];
            foreach(array_keys($fieldNames) as $sgq) {
                if (isset($_SESSION['survey_' . $surveyId][$sgq])) {
                    $updatedValues[$sgq] = $_SESSION['survey_' . $surveyId][$sgq];
                }
            }
            return $updatedValues;
        }
        return array_map(
            function ($element) {
                if (isset($element['value'])) {
                    return $element['value'];
                }
            },
            LimeExpressionManager::singleton()->getUpdatedValues()
        );
    }

    /**
    * Get the complete HTML from a string with Expression for admin
    * @param string $expression : the string to parse
    * @param integer $surveyId : the string to parse
    *
    */
    private static function getHtmlExpression($expression, $surveyId)
    {
        $LEM = LimeExpressionManager::singleton();
        $aReData = array();
        if ($surveyId) {
            $LEM::StartSurvey($surveyId, 'survey', array('hyperlinkSyntaxHighlighting' => true));// replace QCODE
        }
        $expression = $expression;
        LimeExpressionManager::ProcessString($expression);
        return $LEM::GetLastPrettyPrintExpression();
    }
}
