# EquationActionAfterSubmit

Act like an equation after survey submitted, allow to set some value.

## Usage

You find in survey plugin settings a new textarea item, this textarea act like an [equation question type](https://manual.limesurvey.org/Question_type_-_Equation) without databae column.

You can use this equation with [assignment operator](https://manual.limesurvey.org/Expression_Manager#Using_Assignment_Operator).

**Warning** : no control was done on assignment, you can send bad value to database and show SQL error to public.

Support

If you need support on configuration and installation of this plugin : [create a support ticket on support.sondages.pro](https://support.sondages.pro/).

## Contribute and issue

Contribution are welcome, for patch and issue : use [gitlab](https://gitlab.com/SondagesPro/ExpressionManager/EquationActionAfterSubmit).

## Home page & Copyright
- HomePage <http://extensions.sondages.pro/>
- Copyright © 2022 Denis Chenu <https://sondages.pro>
- Licence : GNU Affero General Public License <https://www.gnu.org/licenses/agpl-3.0.html>
- [![Donate](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/SondagesPro/) : [Donate on Liberapay](https://liberapay.com/SondagesPro/) or [Donate directly on support](https://support.sondages.pro/open.php?topicId=12)
